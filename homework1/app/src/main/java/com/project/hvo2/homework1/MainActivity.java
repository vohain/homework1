package com.project.hvo2.homework1;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {


    TextView numMonth5Txt;
    TextView numMonth10Txt;
    TextView numMonth20Txt;
    TextView numMonth30Txt;
    EditText numLoanTxt;
    EditText numRateTxt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        numMonth5Txt = (TextView) findViewById(R.id.numMonth5Txt);
        numMonth10Txt = (TextView) findViewById(R.id.numMonth10Txt);
        numMonth20Txt = (TextView) findViewById(R.id.numMonth20Txt);
        numMonth30Txt = (TextView) findViewById(R.id.numMonth30Txt);
        numLoanTxt = (EditText) findViewById(R.id.numLoanTxt);
        numRateTxt = (EditText) findViewById(R.id.numRateTxt);

        Button calcBtn = (Button) findViewById(R.id.calcBtn);
        calcBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){
                double loanAmount  = Float.parseFloat(numLoanTxt.getText().toString());
                double interest  = Float.parseFloat(numRateTxt.getText().toString());

                double y1 =1.0;
                double y2 =1.0;
                double y3 =1.0;
                double y4 =1.0;
                double temp1 = interest /1200.0;
                double year5 = 5*12;
                double year10 = 10*12;
                double year20 = 20*12;
                double year30 = 30*12;

                for (int i=1; i<= year5; i++) {
                    y1 *= (1 + temp1);
                }
                for (int i=1; i<= year10; i++) {
                    y2 *= (1 + temp1);
                }
                for (int i=1; i<= year20; i++) {
                    y3 *= (1 + temp1);
                }
                for (int i=1; i<= year30; i++) {
                    y4 *= (1 + temp1);
                }

                double result1 = loanAmount*(temp1/(1-(1/y1)));
                double result2 = loanAmount*(temp1/(1-(1/y2)));
                double result3 = loanAmount*(temp1/(1-(1/y3)));
                double result4 = loanAmount*(temp1/(1-(1/y4)));

                String s1 = String.format("%.2f", result1);
                String s2 = String.format("%.2f", result2);
                String s3 = String.format("%.2f", result3);
                String s4 = String.format("%.2f", result4);

                s1 = "5 year : $  "+s1;
                s2 = "10 year : $  "+s2;
                s3 = "20 year : $  "+s3;
                s4 = "30 year : $  "+s4;


                numMonth5Txt.setText(s1);
                numMonth10Txt.setText(s2);
                numMonth20Txt.setText(s3);
                numMonth30Txt.setText(s4);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
